## O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

- Code Review.

- Introduce the various roles and responsibilities in the simulation project.

- Develop an idea according to your needs and report with an elevator speech.

- Use user journey to draw idea.

- Draw the user story card.

- Learn to use kanban.

- Do team Retro.

## R (Reflective): Please use one word to express your feelings about today's class.

   Hard.

## I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

- When we used the elevator speech to introduce our idea, we did not distinguish between good groups and focused on users rather than investors.

- I am not familiar with the user journey, so the drawing does not meet the requirements.

## D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

- In the later study, better teamwork, especially in the simulation project stage, needs our solidarity and mutual help.
